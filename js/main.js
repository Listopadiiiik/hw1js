// 1. Є батьківський клас, і є дочірній клас. Батьківський клас зберігає в собі деякі властивості.
// Всі властивості батьківського класу дочірній клас може наслідувати і мати, так би мовити, в собі.
// Також в дочірній клас ми можемо додати ще властивостей, і тоді дочірній клас буде відрізнятись від батьківського. 

// 2. super() активує конструктор батьківського класу в дочірньому класі.

class Employee {
    constructor(name, age, salary){
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    get prop(){
        return this.age + " year old " + this.name + "'s salary is " + this.salary;
    }
    set prop([val1, val2, val3]){
        this.name = val1;
        this.age = val2;
        this.salary = val3;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang){
        super(name, age, salary)
        this.lang = lang;
    }
    get prop(){
        return this.salary * 3;
    }
}


const pr1 = new Programmer('Nikitos', 17, 2000, 'JavaScript');
const pr2 = new Employee('Magda', 21, 17600);
const pr3 = new Programmer('Jordan', 35, 40999, 'Python');

console.log(pr1);
console.log(pr2);
console.log(pr3);
pr2.prop = ['Igor', 66, 120000];
console.log(pr2.prop)
console.log(pr3.prop);